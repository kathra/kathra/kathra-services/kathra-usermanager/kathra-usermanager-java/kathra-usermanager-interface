/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.usermanager.service;

import org.kathra.core.model.Group;
import org.kathra.core.model.User;
import java.util.List;

public interface UserManagerService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Assign user to group
    * 
    * @param userId User id (required)
    * @param groupPath Group Path (required)
    * @return User
    */
    User assignUserToGroup(String userId, String groupPath) throws Exception;

    /**
    * Create a new group
    * 
    * @param group Group object to be created (required)
    * @return Group
    */
    Group createGroup(Group group) throws Exception;

    /**
    * Create a new user
    * 
    * @param user User object to be created (required)
    * @return User
    */
    User createUser(User user) throws Exception;

    /**
    * Delete group
    * 
    * @param groupPath Group Path (required)
    * @return Group
    */
    Group deleteGroup(String groupPath) throws Exception;

    /**
    * Delete user
    * 
    * @param userId User id (required)
    * @return User
    */
    User deleteUser(String userId) throws Exception;

    /**
    * Return group
    * 
    * @param groupPath Group Path (required)
    * @return Group
    */
    Group getGroup(String groupPath) throws Exception;

    /**
    * Find all groups
    * 
    * @return List<Group>
    */
    List<Group> getGroups() throws Exception;

    /**
    * Return groups from user
    * 
    * @param userId User id (required)
    * @return List<Group>
    */
    List<Group> getGroupsAssignationsFromUser(String userId) throws Exception;

    /**
    * Return user
    * 
    * @param userId User id (required)
    * @return User
    */
    User getUser(String userId) throws Exception;

    /**
    * Find all users
    * 
    * @return List<User>
    */
    List<User> getUsers() throws Exception;

    /**
    * Patch group
    * 
    * @param groupPath Group Path (required)
    * @param group Group object to be created (required)
    * @return Group
    */
    Group patchGroup(String groupPath, Group group) throws Exception;

    /**
    * Patch user
    * 
    * @param userId User id (required)
    * @param user User object to be patched (required)
    * @return User
    */
    User patchUser(String userId, User user) throws Exception;

    /**
    * Unassign user to group
    * 
    * @param userId User id (required)
    * @param groupPath Group Path (required)
    * @return User
    */
    User unassignUserToGroup(String userId, String groupPath) throws Exception;

    /**
    * Update group
    * 
    * @param groupPath Group Path (required)
    * @param group Group object to be created (required)
    * @return Group
    */
    Group updateGroup(String groupPath, Group group) throws Exception;

    /**
    * Update user
    * 
    * @param userId User id (required)
    * @param user User object to be updated (required)
    * @return User
    */
    User updateUser(String userId, User user) throws Exception;

}
